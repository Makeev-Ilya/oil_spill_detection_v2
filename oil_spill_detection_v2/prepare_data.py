import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler


def drop_usls_cols(st_df: pd.DataFrame):
    """Удаление ненужных колонок из датафрейма

    Args:
        st_df (pd.DataFrame): Датафрейм с изучаемыми данными
    """
    useless_columns = ["created_at", "spc_latin", "spc_common"]
    st_df.drop(useless_columns, axis=1, inplace=True)


def drop_na(st_df: pd.DataFrame):
    """Удаление записей хотя бы с одним пропущенным значением

    Args:
        st_df (pd.DataFrame): _description_
    """
    st_df.dropna(inplace=True)


def make_target_col(st_df: pd.DataFrame):
    """Формирование целевой переменной

    Args:
        st_df (pd.DataFrame): Датафрейм с изучаемыми данными
    """
    st_df["health_bool"] = (st_df.health == "Good").astype("int")


def drop_cat_cols(st_df: pd.DataFrame):
    categorical_columns = [key for key in st_df.keys() if st_df[key].dtype == "object"]
    st_df.drop(categorical_columns, axis=1, inplace=True)


def make_splits(st_df: pd.DataFrame):
    """Формирование обучающей, тестовой и валидационной выборок

    Args:
        st_df (pd.DataFrame): Датафрейм с изучаемыми данными
    """
    train, test = train_test_split(st_df, test_size=0.3, random_state=42)
    test, val = train_test_split(test, test_size=0.33, random_state=42)
    st_df = train
    test.to_csv("data/test.csv")
    val.to_csv("data/val.csv")


def normalize_train_df(st_df: pd.DataFrame):
    """Нормализация признаков обучающей выборки

    Args:
        st_df (pd.DataFrame): Датафрейм с изучаемыми данными
    """
    scaler = MinMaxScaler()
    st_df = pd.DataFrame(scaler.fit_transform(st_df), columns=st_df.columns)


def prepare_data():
    """Пайплайн подготовки данных"""
    st_df = pd.read_csv(
        "data/2015-street-tree-census-tree-data.csv", index_col="tree_id"
    )
    drop_usls_cols(st_df)
    drop_na(st_df)
    make_target_col(st_df)
    drop_cat_cols(st_df)
    make_splits(st_df)
    normalize_train_df(st_df)
    st_df.to_csv("data/train.csv")


if __name__ == "__main__":
    prepare_data()
