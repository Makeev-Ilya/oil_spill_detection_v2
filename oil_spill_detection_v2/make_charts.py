import os

import folium
import pandas as pd
import plotly.express as px


def read_pd() -> pd.DataFrame:
    """Загрузка и вывод основных параметров таблицы data/2015-street-tree-census-tree-data.csv.

    Returns:
        pd.DataFrame: Датафрейм данных из таблицы
    """
    for i in ["reports/distributions", "reports/missed_values"]:
        if not os.path.exists(i):
            os.makedirs(i)

    st_df = pd.read_csv(
        "data/2015-street-tree-census-tree-data.csv", index_col="tree_id"
    )
    print("Data preview:")
    print(st_df.head())
    print(st_df.info())
    return st_df


def make_na_pies(st_df: pd.DataFrame):
    """Формирование и сохранение pie-chart долей пропущенных значений в /reports/missed_values

    Args:
        st_df (pd.DataFrame): Датафрейм с изучаемыми данными
    """
    columns_with_na = st_df.columns[st_df.isnull().any()].tolist()
    for col in columns_with_na:
        missing_values_count = st_df[col].isnull().sum()
        fig = px.pie(
            names=["Пропущенные значения", "Заполненные значения"],
            values=[missing_values_count, len(st_df) - missing_values_count],
            title="Доля пропущенных значений в колонке " + col,
        )
        fig.write_html("reports/missed_values/" + col + "_missed_values.html")
    print("Missed values pie charts saved at /reports/missed_values")


def make_distirbutions_graphs(st_df: pd.DataFrame, continuous_columns: list[str]):
    """Формирование и сохранение графиков распределений указанных вещественных признаков

    Args:
        st_df (pd.DataFrame): Датафрейм с изучаемыми данными
        continuous_columns (list[str]): Список вещественных признаков
    """
    for col_i in continuous_columns[:5]:
        for col_j in continuous_columns[:5]:
            if col_i != col_j:
                fig = px.scatter(st_df, x=col_i, y=col_j)
                fig.write_html(
                    "reports/distributions/" + col_i + "_and_" + col_j + ".html"
                )
    print("Distributions saved at /reports/distributions")


def make_corr_matrix_graphs(st_df: pd.DataFrame, continuous_columns: list[str]):
    """Формирование и сохранение матрицы корреляции указанных вещественных признаков

    Args:
        st_df (pd.DataFrame): Датафрейм с изучаемыми данными
        continuous_columns (list[str]): писок вещественных признаков
    """
    fig = px.imshow(st_df[continuous_columns].corr(), text_auto=True)
    fig.write_html("reports/corr_matrix.html")
    print("Correlation matrix saved at /reports/corr_matrix.html")


def make_map(st_df: pd.DataFrame):
    """Формирование и сохранение карты расположения деревьев (или других объектов) в Нью-Йорке

    Args:
        st_df (pd.DataFrame): Датафрейм с изучаемыми данными
    """
    map = folium.Map(zoom_start=10, location=[40.6, -74.5])
    for i in st_df.index[: 683788 // 1000]:
        folium.Marker(
            location=[st_df.loc[i].latitude, st_df.loc[i].longitude],
            popup=i,
        ).add_to(map)
    map.save("reports/map.html")
    print("Tree map saved at reports/map.html")


def make_charts():
    """Пайплайн визуализации данных"""
    st_df = read_pd()
    make_na_pies(st_df)
    continuous_columns = [
        key for key in st_df.keys() if st_df[key].dtype in ("int64", "float64")
    ]
    make_distirbutions_graphs(st_df, continuous_columns)
    make_corr_matrix_graphs(st_df, continuous_columns)
    make_map(st_df)


if __name__ == "__main__":
    make_charts()
