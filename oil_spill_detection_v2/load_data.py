import os


def load_data(directory, filename):
    file_path = os.path.join(directory, filename)

    if os.path.exists(file_path):
        print(f"Dataset {filename} was found")
    else:
        os.system(
            f"kaggle datasets download -d new-york-city/ny-2015-street-tree-census-tree-data -p {directory}",
        )
        print(f"Dataset {filename} was downloaded to /data.")
        os.system(
            f"unzip {directory}/ny-2015-street-tree-census-tree-data.zip -d {directory}"
        )
        os.system(f"rm {directory}/ny-2015-street-tree-census-tree-data.zip")


directory = "../data"
filename = "2015-street-tree-census-tree-data.csv"

if __name__ == "__main__":
    directory = "./data"
    load_data(directory, filename)
