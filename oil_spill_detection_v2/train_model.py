import pandas as pd
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score


def train_model():
    train = pd.read_csv("data/train.csv", index_col="tree_id")
    test = pd.read_csv("data/test.csv", index_col="tree_id")

    X_train = train.drop("health_bool", axis=1)
    y_train = train.health_bool
    X_test = test.drop("health_bool", axis=1)
    y_test = test.health_bool

    clf = LogisticRegression()
    clf.fit(X_train, y_train)
    predictions = clf.predict(X_test)
    score = accuracy_score(y_test, predictions)
    print("model score:", score)


if __name__ == "__main__":
    train_model()
