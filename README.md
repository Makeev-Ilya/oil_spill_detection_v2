# Установка зависимостей
## pip
`pip install -r requirements.txt`

## poetry
- `poetry install` - установить все зависимости
- `poetry install --only dev` - установить зависимости только из группы dev
- `poetry install --only prod` - установить зависимости только из группы prod

# Использование Docker
1. `docker build . -t osd_image` - собрать контейнер
2. `docker run osd_image`
