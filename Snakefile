rule load_data:
  output:
    "data/2015-street-tree-census-tree-data.csv"
  script:
    "oil_spill_detection_v2/load_data.py"

rule make_charts:
  input:
    "data/2015-street-tree-census-tree-data.csv"
  output:
    "reports/distributions/block_id_and_postcode.html"
  "reports/distributions/block_id_and_stump_diam.html"
  "reports/distributions/block_id_and_tree_dbh.html"
  "reports/distributions/block_id_and_tree_id.html"
  "reports/distributions/postcode_and_block_id.html"
  "reports/distributions/postcode_and_stump_diam.html"
  "reports/distributions/postcode_and_tree_dbh.html"
  "reports/distributions/postcode_and_tree_id.html"
  "reports/distributions/stump_diam_and_block_id.html"
  "reports/distributions/stump_diam_and_postcode.html"
  "reports/distributions/stump_diam_and_tree_dbh.html"
  "reports/distributions/stump_diam_and_tree_id.html"
  "reports/distributions/tree_dbh_and_block_id.html"
  "reports/distributions/tree_dbh_and_postcode.html"
  "reports/distributions/tree_dbh_and_stump_diam.html"
  "reports/distributions/tree_dbh_and_tree_id.html"
  "reports/distributions/tree_id_and_block_id.html"
  "reports/distributions/tree_id_and_postcode.html"
  "reports/distributions/tree_id_and_stump_diam.html"
  "reports/distributions/tree_id_and_tree_dbh.html"
  "reports/missed_values/bbl_missed_values.html"
  "reports/missed_values/bin_missed_values.html"
  "reports/missed_values/census tract_missed_values.html"
  "reports/missed_values/council district_missed_values.html"
  "reports/missed_values/guards_missed_values.html"
  "reports/missed_values/health_missed_values.html"
  "reports/missed_values/problems_missed_values.html"
  "reports/missed_values/sidewalk_missed_values.html"
  "reports/missed_values/spc_common_missed_values.html"
  "reports/missed_values/spc_latin_missed_values.html"
  "reports/missed_values/steward_missed_values.html"
  "reports/corr_matrix.html"
  "reports/map.html"
  script:
    "oil_spill_detection_v2/make_charts.py"

rule prepare_data:
  input:
    "data/2015-street-tree-census-tree-data.csv"
  output:
    "data/test.csv", "data/val.csv", "data/train.csv"
  script:
    "oil_spill_detection_v2/prepare_data.py"

rule train_model:
  input:
    "data/train.csv", "data/train.csv"
  script:
    "oil_spill_detection_v2/train_model.py"