from oil_spill_detection_v2.load_data import load_data
from oil_spill_detection_v2.make_charts import make_charts
from oil_spill_detection_v2.prepare_data import prepare_data


def main():
    load_data()
    make_charts()
    prepare_data()


if __name__ == "__main__":
    main()
