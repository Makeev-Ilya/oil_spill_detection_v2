FROM python:3.11

WORKDIR /app

COPY requirements.txt ./
COPY pyproject.toml ./
COPY poetry.lock ./

RUN pip install poetry && pip install plotly && poetry install

# COPY ./test.py ./

ENTRYPOINT [ "poetry", "run", "python", "main.py" ]